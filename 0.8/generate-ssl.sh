sed -i "s|commonName.*|commonName=${FQDN}|g" openssl-server.cnf
touch index.txt && echo '01' > serial.txt && \
    openssl req -x509 -config openssl-ca.cnf -newkey rsa:4096 -sha256 -nodes -days 36500 -subj $SUBJ -out platform.pem -outform PEM && \
    openssl req -out $FQDN.csr -new -newkey rsa:2048 -nodes -keyout $FQDN.key -config openssl-server.cnf -subj "$SUBJ/CN=$FQDN" && \
    openssl ca -batch -config openssl-ca-sign.cnf -policy signing_policy -extensions signing_req -out $FQDN.pem -infiles $FQDN.csr
