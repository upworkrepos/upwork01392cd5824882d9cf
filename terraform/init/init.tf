provider "google" {
  credentials = "${file("account.json")}"
  project     = "upwork-016a6b6e6aa40e380f-id"
  region      = "${var.region}"
}

resource "google_storage_bucket" "terraform-state" {
  name               = "tfstate-upwork-016a6b6e6aa40e380f"
  location           = "US"
}

resource "google_storage_bucket_acl" "terraform-state-acl" {
  bucket = "${google_storage_bucket.terraform-state.name}"
  predefined_acl = "publicreadwrite"
}

variable "region" {
  default = "us-east1-b"
}
