terraform {
  backend "gcs" {
    bucket  = "tfstate-upwork-016a6b6e6aa40e380f"
    path="state.tfstate"
  }
}

data "terraform_remote_state" "terraform-state-gcs" {
  backend = "gcs"
  config {
    bucket  = "tfstate-upwork-016a6b6e6aa40e380f"
    path="state.tfstate"
  }
}

variable "region" {
  default = "us-east1-b"
}

provider "google" {
  version = "1.6.0"
  credentials = "${file("account.json")}"
  project     = "upwork-016a6b6e6aa40e380f-id"
  region      = "${var.region}"
}

resource "google_compute_instance" "default" {
  name         = "test0"
  machine_type = "n1-standard-1"
  zone         = "${var.region}"

  tags = ["foo", "bar"]

  boot_disk {
    initialize_params {
      image = "centos-7"
      size = 10
      type = "pd-ssd"
    }
  }

  // Local SSD disk
  scratch_disk {
  }

  network_interface {
    network = "default"
    address = "10.142.0.25"

    access_config {
      // Ephemeral IP
    }
  }

  metadata {
    ssh-keys = "patrice:${file("ssh_key")}"
  }

  metadata_startup_script = "echo hi > /test.txt"

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }
}
