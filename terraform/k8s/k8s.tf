variable "region" {
  default = "us-central1-c"
}

provider "google" {
  credentials = "${file("account.json")}"
  project     = "upwork-016a6b6e6aa40e380f-id"
  region      = "${var.region}"
}

resource "google_container_cluster" "primary" {
  name = "upwork-016a6b6e6aa40e380f-k8s"
  zone = "${var.region}"
  initial_node_count = 1

  master_auth {
    username = "user"
    password = "upwork-016a6b6e6aa40e380f-k8s-pwd"
  }

  node_config {
    machine_type = "n1-highcpu-8"
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring"
    ]
  }
}
