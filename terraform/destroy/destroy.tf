terraform {
  backend "gcs" {
    bucket  = "tfstate-upwork-016a6b6e6aa40e380f"
    prefix  = "terraform/state"
  }
}

data "terraform_remote_state" "terraform-state-gcs" {
  backend = "gcs"
  config {
    bucket  = "tfstate-upwork-016a6b6e6aa40e380f"
    prefix  = "terraform/state"
  }
}

variable "region" {
  default = "us-east1-b"
}

provider "google" {
  credentials = "${file("account.json")}"
  project     = "upwork-016a6b6e6aa40e380f-id"
  region      = "${var.region}"
}
